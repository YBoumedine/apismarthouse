package ca.uqam;

import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.SocketType;
import org.json.*;
import org.mindrot.jbcrypt.BCrypt;
import java.io.FileWriter;
import java.io.File;
import org.apache.commons.io.FileUtils;
import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.UUID;
import java.lang.Math;
import com.sendgrid.*;

public class SmartHouse {
	static List<String> user_cookies = new ArrayList<>();

	public static void main(String [] args) {
		ThreadManager threads = new ThreadManager(new Server(), new Application());
		threads.run();
	}

	static class ThreadManager{

		Thread appThread;
		Thread serverThread;

		public ThreadManager(Runnable serverRunnable, Runnable appRunnable){
			this.appThread = new Thread(appRunnable);
			this.serverThread = new Thread(serverRunnable);
		}
		public void run(){
			this.serverThread.run();
			this.appThread.run();
		}
	}

	static class Application implements Runnable{
		JSONObject requests = new JSONObject();
		JSONObject status = new JSONObject();
		Float target_temperature = null;
		Float temperature = null;
		int activity = 0;
		String time = "";
		boolean mail_sent = false;


		public void run(){
			try{
				requests = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/requests.json"), "utf-8"));
				status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
			}catch(IOException e){
				e.printStackTrace();
			}
			try (ZContext context = new ZContext()) {
				ZMQ.Socket subscriber = context.createSocket(SocketType.SUB);
				subscriber.connect("tcp://*:5555");
				subscriber.subscribe("temperature");
				subscriber.subscribe("activity");
				subscriber.subscribe("time");
				subscriber.subscribe("door_lock_sensor");


				ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
				publisher.bind("tcp://*:6666");

				while(!Thread.currentThread().isInterrupted()){
					try{
						requests = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/requests.json"), "utf-8"));
						status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
					}catch(IOException e){
						e.printStackTrace();
					}
					String topic = subscriber.recvStr();
					String data = subscriber.recvStr();

					if(topic.equals("temperature")){

						//Manipulating the json stuff
						target_temperature = requests.getFloat("thermostat_request");
						temperature = Float.parseFloat(data);
						status.put("thermostat", target_temperature);
						status.put("temperature", data);
						try(FileWriter writeStatus = new FileWriter("ca/uqam/json_files/status.json")){
							writeStatus.write(status.toString());
						}catch(IOException e){
							e.printStackTrace();
						}
						System.out.println(topic + " : " + data);

						//Send mail if needed
						if(temperature > 30 && !mail_sent){
							try {
								SendMail();
							}catch(IOException e){
								e.printStackTrace();
							}
							mail_sent = true;
						}else if(temperature <= 30 ){
							mail_sent = false;
						}

						//Publishing stuff here
						if( temperature < (target_temperature-2)){
							System.out.println("Turning heater on.");
							publisher.send("heater", ZMQ.SNDMORE);
							publisher.send("on");
						}else if(temperature > (target_temperature+2) || (temperature >= target_temperature-2 &&temperature <= target_temperature+2 )){
							System.out.println("Turning heater off.");
							publisher.send("heater", ZMQ.SNDMORE);
							publisher.send("off");
						}
						if(temperature < (target_temperature+2)){
							System.out.println("Turning ac off.");
							publisher.send("ac", ZMQ.SNDMORE);
							publisher.send("off");
						}else if(temperature > (target_temperature+2)){
							System.out.println("Turning ac on.");
							publisher.send("ac", ZMQ.SNDMORE);
							publisher.send("on");
						}
					}
					if(topic.equals("activity")){
						activity = Integer.parseInt(data);
						status.put("presence", Integer.parseInt(data));
						try(FileWriter writeStatus = new FileWriter("ca/uqam/json_files/status.json")){
							writeStatus.write(status.toString());
						}catch(IOException e){
							e.printStackTrace();
						}

						System.out.println(topic + " : " + data);
						System.out.println(activity);
						if(activity != 0){
							System.out.println("Turning lights on.");
							publisher.send("lights", ZMQ.SNDMORE);
							publisher.send("on");
						}else{
							System.out.println("Turning lights off.");
							publisher.send("lights", ZMQ.SNDMORE);
							publisher.send("off");
						}
					}
					if(topic.equals("time")){
						boolean door_lock_request = requests.getBoolean("door_lock_request");
						time = data;
						System.out.println(topic + " : " + data);
						if(data.equals("23:00:00") || door_lock_request){
							System.out.println("Turning door lock on.");
							publisher.send("door_lock", ZMQ.SNDMORE);
							publisher.send("on");
						}else if (data.equals("7:00:00") || !door_lock_request){
							System.out.println("Turning door lock off.");
							publisher.send("door_lock", ZMQ.SNDMORE);
							publisher.send("off");
						}
					}

					if(topic.equals("door_lock_sensor")){
						status.put("door_lock_sensor", data);
						try(FileWriter writeStatus = new FileWriter("ca/uqam/json_files/status.json")){
							writeStatus.write(status.toString());
						}catch(IOException e){
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	static class Server implements Runnable{
		public void run(){
			try{
				HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
				server.createContext("/thermostat", new ThermostatHandler());
				server.createContext("/door_lock", new DoorLockHandler());
				server.createContext("/temperature", new TemperatureHandler());
				server.createContext("/presence", new PresenceHandler());
				HttpContext authContext = server.createContext("/auth", new AuthenticationHandler());
				authContext.setAuthenticator(new Authenticator("auth"));
				server.setExecutor(Executors.newFixedThreadPool(15));
				server.start();
			}catch(Exception ex){
				System.out.println("Error");
			}
		}
	}

	static class ThermostatHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			String requestMethod = he.getRequestMethod();
			String cookieString = he.getRequestHeaders().getFirst("Cookie");
			String response = "";
			int responseCode = 0;

			if(cookieString != null){
				String[] cookieToken = cookieString.split("=");
				if(user_cookies.contains(cookieToken[1])){
					responseCode = 200;
					if (requestMethod.equalsIgnoreCase("GET")) {
						JSONObject status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
						response = status.get("thermostat").toString();

					}else if(requestMethod.equalsIgnoreCase("PUT")){
						Float target_temperature = Float.parseFloat(queryToMap(he.getRequestURI().getQuery()).get("thermostat_request"));
						JSONObject requests = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/requests.json"), "utf-8"));
						requests.put("thermostat_request",target_temperature);

						try(FileWriter writeRequests = new FileWriter("ca/uqam/json_files/requests.json")){
							writeRequests.write(requests.toString());
						}catch(IOException e){
							e.printStackTrace();
						}
						response = requests.toString();
					}
				}else{
					response = "Invalid cookie.";
					responseCode = 401;
				}
			}else{
				response = "No cookie.";
				responseCode = 401;
			}
			addResponseHeader(he);
			he.sendResponseHeaders(responseCode, response.length());
			try (OutputStream os = he.getResponseBody()) {
				os.write(response.getBytes());
			}
		}
	}

	static class TemperatureHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			String requestMethod = he.getRequestMethod();
			String cookieString = he.getRequestHeaders().getFirst("Cookie");
			String response = "";
			int responseCode = 0;

			if(cookieString != null){
				String[] cookieToken = he.getRequestHeaders().getFirst("Cookie").split("=");
				if(user_cookies.contains(cookieToken[1])){
					responseCode = 200;
					if (requestMethod.equalsIgnoreCase("GET")) {
						JSONObject status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
						response = status.get("temperature").toString();
					}
				}else{
					response = "Invalid cookie.";
					responseCode = 401;
				}
			}else{
				response = "No cookie.";
				responseCode = 401;
			}
			addResponseHeader(he);
			he.sendResponseHeaders(responseCode, response.length());
			try (OutputStream os = he.getResponseBody()) {
				os.write(response.getBytes());
			}
		}
	}

	static class DoorLockHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			String requestMethod = he.getRequestMethod();
			String cookieString = he.getRequestHeaders().getFirst("Cookie");
			String response = "";
			int responseCode = 0;

			if(cookieString != null){
				String[] cookieToken = cookieString.split("=");
				if(user_cookies.contains(cookieToken[1])){
					responseCode = 200;
					if(requestMethod.equalsIgnoreCase("GET")) {
						JSONObject status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
						response = status.get("door_lock").toString();

					}else if(requestMethod.equalsIgnoreCase("PUT")){
						boolean door_lock_request = Boolean.parseBoolean(queryToMap(he.getRequestURI().getQuery()).get("door_lock_request"));
						JSONObject requests = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/requests.json"), "utf-8"));
						requests.put("door_lock_request",door_lock_request);

						try(FileWriter writeRequests = new FileWriter("ca/uqam/json_files/requests.json")){
							writeRequests.write(requests.toString());
						}catch(IOException e){
							e.printStackTrace();
						}
						response = requests.toString();
					}
				}else{
					response = "Invalid cookie.";
					responseCode = 401;
				}
			}else{
				response = "No cookie.";
				responseCode = 401;
			}

			addResponseHeader(he);
			he.sendResponseHeaders(responseCode, response.length());
			try (OutputStream os = he.getResponseBody()) {
				os.write(response.getBytes());
			}
		}
	}

	static class PresenceHandler implements HttpHandler {

		@Override
		public void handle(HttpExchange he) throws IOException {
			String requestMethod = he.getRequestMethod();
			String cookieString = he.getRequestHeaders().getFirst("Cookie");
			String response = "";
			int responseCode = 0;

			if(cookieString != null){
				String[] cookieToken = cookieString.split("=");
				if(user_cookies.contains(cookieToken[1])){
					responseCode = 200;
					if (requestMethod.equalsIgnoreCase("GET")) {
						JSONObject status = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/status.json"), "utf-8"));
						response = status.get("presence").toString();

					}
				}else{
					response = "Invalid cookie.";
					responseCode = 401;
				}
			}else{
				response = "No cookie.";
				responseCode = 401;
			}

			addResponseHeader(he);
			he.sendResponseHeaders(responseCode, response.length());
			try (OutputStream os = he.getResponseBody()) {
				os.write(response.getBytes());
			}
		}
	}

	static class AuthenticationHandler implements HttpHandler{
		@Override
		public void handle(HttpExchange he) throws IOException{
			String response = "Authorized. You got yourself a cookie, better use it!";
			String token = UUID.randomUUID().toString();
			user_cookies.add(token);
			addResponseHeader(he);
			he.getResponseHeaders().set("Set-Cookie", "SESSID="+token);
			he.sendResponseHeaders(200, response.length());
			try (OutputStream os = he.getResponseBody()) {
				os.write(response.getBytes());
			}
		}
	}

	static class Authenticator extends BasicAuthenticator{

		public Authenticator(String realm){
			super(realm);
		}

		public boolean checkCredentials(String username, String pwd){
			try{
				JSONObject users = new JSONObject(FileUtils.readFileToString(new File("ca/uqam/json_files/users.json"), "utf-8"));
				String password = users.getString("inf");
				if (BCrypt.checkpw(pwd, password)){
					return true;
				}
			}catch(IOException e){
				e.printStackTrace();
			}
			return false;
		}
	}

	private static void addResponseHeader(HttpExchange httpExchange) {
		List<String> contentTypeValue = new ArrayList<>();
		contentTypeValue.add("application/json");
		httpExchange.getResponseHeaders().put("Content-Type", contentTypeValue);
	}

	public static void SendMail()throws IOException{
		Email from = new Email("test@example.com");
		String subject = "Danger! Temperature rising!";
		Email to = new Email("inf4375.2018@gmail.com");
		Content content = new Content("text/plain", "Careful! The temperature of the house is over 30 degrees!");
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
		Request request = new Request();
		try{
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println("Status :" + response.getStatusCode());
		}catch(IOException e){
			throw e;
		}

		try{
			Request stats_requests = new Request();
			stats_requests.setMethod(Method.GET);
			stats_requests.setEndpoint("stats");
			stats_requests.addQueryParam("start_date", "2018-11-11");
			Response stats_response = sg.api(stats_requests);
			JSONArray response = new JSONArray(stats_response.getBody());
			System.out.println(response.toString(4));
		}catch(Exception e){
			throw e;
		}
	}

	/*
	This is not cheating btw.
	Source : https://stackoverflow.com/questions/11640025/how-to-obtain-the-query-string-in-a-get-with-java-httpserver-httpexchange
	*/

	public static Map<String, String> queryToMap(String query) {
		Map<String, String> result = new HashMap<>();
		for (String param : query.split("&")) {
			String[] entry = param.split("=");
			if (entry.length > 1) {
				result.put(entry[0], entry[1]);
			}else{
				result.put(entry[0], "");
			}
		}
		return result;
	}
}
